This project contains the code to reproduce the results of the Ocean recharge
oscillator proposed in Jin et al., 1997.

The project contains the following files:

- stability.ipynb: Code that plots the graph of the stability regions of the
                   Runge-Kutta 4th and 2nd schemes.
                    
- Functions.ipynb: Code that contains the parameters and the functions required
                   to apply the Runge-Kutta 4th order scheme to solve the
                   coupled PDEs of the SST and thermocline depth.
                  
- Project_1.ipynb: Code that generates the plots used in the report.

There is also the report of the ocean recharge oscillator in pdf format, 
previously uploaded in BlackBoard.